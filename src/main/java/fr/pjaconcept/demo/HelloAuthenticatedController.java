package fr.pjaconcept.demo;

import java.time.LocalDateTime;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class HelloAuthenticatedController {

    @GetMapping("/hello")
    public String hello(@AuthenticationPrincipal UserDetails userDetails) {
        return "Hello '%s' in AUTHENTICATED World, you have the following roles: %s%nInformation valid as of: %s"
                .formatted(userDetails.getUsername(), userDetails.getAuthorities(), LocalDateTime.now());
    }
}
