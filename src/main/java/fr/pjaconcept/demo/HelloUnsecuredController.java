package fr.pjaconcept.demo;

import java.time.LocalDateTime;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/unsecured")
public class HelloUnsecuredController {

    @GetMapping("/hello")
    public String hello(@AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails == null) {
            return "Hello 'Anonymous' in UNSECURED World, you have no roles\nInformation valid as of: %s"
                    .formatted(LocalDateTime.now());
        }

        return "Hello '%s' in UNSECURED World, you have the following roles: %s\nInformation valid as of: %s"
                .formatted(userDetails.getUsername(), userDetails.getAuthorities(), LocalDateTime.now());
    }

    @GetMapping("/whoami")
    @PreAuthorize("isAuthenticated()")
    public String whoami(@AuthenticationPrincipal UserDetails userDetails) {
        return "Your are '%s', you have the following roles: %s\nInformation valid as of: %s"
                .formatted(userDetails.getUsername(), userDetails.getAuthorities(), LocalDateTime.now());
    }

    @GetMapping("/config")
    @PreAuthorize("hasRole('ADMIN')")
    public String getSecret(@AuthenticationPrincipal UserDetails userDetails) {
        return "Your are '%s', you have the following roles: %s, you can get the config\nInformation valid as of: %s"
                .formatted(userDetails.getUsername(), userDetails.getAuthorities(), LocalDateTime.now());
    }
}
