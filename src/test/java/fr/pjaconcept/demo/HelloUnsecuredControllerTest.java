package fr.pjaconcept.demo;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(HelloUnsecuredController.class)
@Import(RouteConfiguration.class)
class HelloUnsecuredControllerTest {
    private static final String BASE_URL = "/api/v1/unsecured";

    @Autowired
    private MockMvc mockMvc;

    @Nested
    class hello {
        private static final String URL = BASE_URL + "/hello";

        @Test
        @WithAnonymousUser
        void should_returnOK_whenUserIsAnonymous() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
        @Test
        @WithMockUser
        void should_returnOK_whenUserIsIdentified() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
    }

    @Nested
    class whoami {
        private static final String URL = BASE_URL + "/whoami";

        @Test
        @WithAnonymousUser
        void should_returnUnauthorized_whenUserIsAnonymous() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isUnauthorized()).andDo(print());
        }
        @Test
        @WithMockUser
        void should_returnOK_whenUserIsIdentified() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
    }

    @Nested
    class config {
        private static final String URL = BASE_URL + "/config";

        @Test
        @WithAnonymousUser
        void should_returnUnauthorized_whenUserIsAnonymous() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isUnauthorized()).andDo(print());
        }
        @Test
        @WithMockUser
        void should_returnForbidden_whenUserIsNotAdmin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isForbidden()).andDo(print());
        }
        @Test
        @WithMockUser(roles = {"ADMIN"})
        void should_returnOK_whenUserIsAdmin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
    }
}
