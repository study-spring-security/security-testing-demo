package fr.pjaconcept.demo;

import java.time.LocalDateTime;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/secured")
public class HelloSecuredController {

    @GetMapping("/hello")
    public String get(@AuthenticationPrincipal UserDetails userDetails) {
        return "Hello '%s' in SECURED World, you have the following roles: %s%nInformation valid as of: %s"
                .formatted(SecurityFacade.getName(), userDetails.getAuthorities(), LocalDateTime.now());
    }

    @GetMapping("/classified")
    public String getSecret(@AuthenticationPrincipal Jwt jwt) {
        SecurityFacade.requirePrivilege("classified:read");
        return "Hello '%s' in the SECURE world, you have the following privileges: %s%nThe secret is: 42%nInformation valid as of: %s"
                .formatted(SecurityFacade.getName(), jwt.getClaimAsString(SecurityFacade.PRIVILEGES_CLAIM), LocalDateTime.now());
    }
}
