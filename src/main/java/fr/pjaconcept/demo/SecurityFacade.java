package fr.pjaconcept.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public class SecurityFacade {

    public static final String PRIVILEGES_CLAIM = "PRIVILEGES";

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static User getPrincipal() {
        return (User) getAuthentication().getPrincipal();
    }

    public static String getName() {
        return getAuthentication().getName();
    }

    public static List<String> getPrivileges() {
        return getJwt().getClaimAsStringList(PRIVILEGES_CLAIM);
    }

    public static void requirePrivilege(String Privilege) {
        if (!getPrivileges().contains(Privilege)) {
            throw new AccessDeniedException("required Privilege not included in caller's granted Privileges: " + Privilege);
        }
    }

    public static void requireAllPrivileges(String... Privileges) {
        //noinspection SlowListContainsAll
        if(!getPrivileges().containsAll(List.of(Privileges))){
            throw new AccessDeniedException("all required Privileges not included in caller's granted Privileges: " +
                                            Arrays.toString(Privileges));
        }
    }

    public static Jwt getJwt() {
        var authentication = getAuthentication();

        if (authentication == null) {
            throw new SecurityException("no Authentication found in HTTP request");
        }
        if(authentication instanceof JwtAuthenticationToken jwtAuthenticationToken) {
            return jwtAuthenticationToken.getToken();
        }
        if(authentication.getCredentials() instanceof JwtAuthenticationToken jwtAuthenticationToken) {
            return jwtAuthenticationToken.getToken();
        }
        if (authentication.getCredentials() instanceof Jwt jwt) {
            return jwt;
        }

        throw new SecurityException("no JWT found in HTTP request");
    }
}
