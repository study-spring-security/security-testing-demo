package fr.pjaconcept.demo;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@WebMvcTest(HelloAuthenticatedController.class)
@Import(RouteConfiguration.class)
class HelloAuthenticatedControllerTest {
    private static final String BASE_URL = "/api/v1/auth";

    @Autowired
    private MockMvc mockMvc;

    @Nested
    class hello {
        private static final String URL = BASE_URL + "/hello";

        @Test
        @WithAnonymousUser
        void should_returnUnauthorized_whenUserIsAnonymous() throws Exception {
            // Unauthorized because not identified access are not tolerated
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isUnauthorized()).andDo(print());
        }

        @Test
        @WithMockUser
        void should_returnOk_whenUserIsIdentified() throws Exception {
            // Ok because the user is identified
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
    }

}
