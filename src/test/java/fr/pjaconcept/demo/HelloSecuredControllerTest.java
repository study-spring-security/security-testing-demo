package fr.pjaconcept.demo;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@WebMvcTest(HelloSecuredController.class)
@Import(RouteConfiguration.class)
class HelloSecuredControllerTest {
    private static final String BASE_URL = "/api/v1/secured";

    @Autowired
    private MockMvc mockMvc;

    @Nested
    class hello {
        private static final String URL = BASE_URL + "/hello";

        @Test
        @WithAnonymousUser
        void should_returnUnauthorized_whenUserIsAnonymous() throws Exception {
            // Unauthorized because not identified are not tolerated
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isUnauthorized()).andDo(print());
        }

        @Test
        @WithMockUser
        void should_returnForbidden_whenUserIsNotAdmin() throws Exception {
            // Forbidden because the user is identified by does not have enough rights
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isForbidden()).andDo(print());
        }

        @Test
        @WithMockUser(roles = {"ADMIN"})
        void should_returnOK_whenUserIsAdmin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andDo(print());
        }
    }

    @Nested
    class classified {
        private static final String URL = BASE_URL + "/classified";

        @Test
        @WithMockUser(roles = {"ADMIN"})
        void should_returnForbidden_whenRoleNotConfiguredInJwt() throws Exception {
            // Demonstrate that the user characteristics put on the @WithMockUser annotation
            // are ineffective when setting a Jwt via a RequestPostProcessors.
            // The RequestPostProcessors will replace the UsernamePasswordAuthenticationToken created
            // by @WithMockUser in the SecurityContextHolder by a JwTAuthenticationToken when it runs.
            var requestBuilder = MockMvcRequestBuilders.get(URL)
                    .with(jwt().jwt((builder) -> builder.claim(SecurityFacade.PRIVILEGES_CLAIM, "classified:read")));

            mockMvc.perform(requestBuilder).andExpect(status().isForbidden()).andDo(print());
        }

        @Test
        void should_returnForbidden_whenRolesConfiguredInJwtButNotEnoughPrivilege() throws Exception {
            var requestBuilder = MockMvcRequestBuilders.get(URL)
                    .with(jwt().jwt((builder) -> builder.claim(SecurityFacade.PRIVILEGES_CLAIM, "declassified:read"))
                            .authorities(new SimpleGrantedAuthority("ROLE_ADMIN"))); // this is required

            mockMvc.perform(requestBuilder).andExpect(status().isForbidden()).andDo(print());
        }

        @Test
        void should_returnOK_whenRolesConfiguredInJwtAndEnoughPrivilege() throws Exception {
            var requestBuilder = MockMvcRequestBuilders.get(URL)
                    .with(jwt().jwt((builder) -> builder.claim(SecurityFacade.PRIVILEGES_CLAIM, "classified:read"))
                            .authorities(new SimpleGrantedAuthority("ROLE_ADMIN"))); // this is required

            mockMvc.perform(requestBuilder).andExpect(status().isOk()).andDo(print());
        }
    }
}
